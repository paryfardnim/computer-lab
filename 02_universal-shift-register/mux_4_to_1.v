module mux_4_to_1 (I3,
            I2,
            I1,
            I0,
            s0,
            s1,
            out);
    input wire I3, I2, I1, I0;
    input wire s0, s1;
    output reg out;
    always @(I3 or I2 or I1 or I0 or s0 or s1)
    begin
        case ({s0 , s1})
            2'b00 : out <= I3;
            2'b01 : out <= I2;
            2'b10 : out <= I1;
            2'b11 : out <= I0;
        endcase
    end
    
endmodule
