// falling edge D flip flop
module d_flip_flop(D,
                   Q,
                   clk,
                   reset);
    input D;
    input clk;
    input reset;
    output reg Q;
    always @(negedge clk)
    begin
        if (reset == 1'b1)
            Q <= 1'b0;
        else
            Q <= D;
    end
endmodule
