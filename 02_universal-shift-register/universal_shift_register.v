module universal_shift_register(A,
                                I,
                                clk,
                                reset,
                                s,
                                SIFSR,  // serial input for shift right
                                SIFSL); // serial input for shift left
    wire [3:0] w;
    input [3:0]I;
    input [1:0]s ;
    input clk;
    input reset;
    input SIFSR;
    input SIFSL;
    output [3:0]A;
    
    // multiplexers
    mux_4_to_1 mux_1(I[0],SIFSL,A[1],A[0],s[0],s[1],w[0]);
    mux_4_to_1 mux_2(I[1],A[0],A[2],A[1],s[0],s[1],w[1]);
    mux_4_to_1 mux_3(I[2],A[1],A[3],A[2],s[0],s[1], w[2]);
    mux_4_to_1 mux_4(I[3],A[2],SIFSR,A[3],s[0],s[1],w[3]);
    
    //D flip flops
    d_flip_flop d_1(w[0],A[0],clk,reset);
    d_flip_flop d_2(w[1],A[1],clk,reset);
    d_flip_flop d_3(w[2],A[2],clk,reset);
    d_flip_flop d_4(w[3],A[3],clk,reset);
    
endmodule


