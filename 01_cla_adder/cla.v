module cla(input c_in,
           input [3:0] p,
           input [3:0] g,
           output c_out,
           output pg,
           output gg,
           );
    
    assign pg    = p[0]&p[1]&p[2]&p[3];
    assign gg    = g[3] | g[2]&p[3] | g[1]&p[3]&p[2] | g[0]&p[3]&p[2]&p[1];
    assign c_out = gg + pg&c_in;
endmodule
