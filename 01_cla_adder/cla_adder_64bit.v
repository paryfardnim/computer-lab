// test case a = 4'b0100 , b = 4'b0101 , c_out = 1'b0
module cla_adder_64bit(input [63:0] a,
                      input [63:0] b,
                      input c_in,
                      output [63:0] s,
                      output c_out);

wire [3:0] p;
wire [3:0] g;
wire [4:0] c;


// 4 4bit-full adders for 16 bit cla
full_adder_16bit cla_adder_16bit_0(a[15:0] , b[15:0] , c_in , s[15:0] , c[1] , p[0] , g[0]);
full_adder_16bit cla_adder_16bit_1(a[16:31] , b[16:31] , c[1] , s[16:31] , c[2] , p[1] , g[1]);
full_adder_16bit cla_adder_16bit_2(a[32:47] , b[32:47] , c[2] , s[32:47] , c[3] , p[2] , g[2]);
full_adder_16bit cla_adder_16bit_3(a[63:48] , b[63:48] , c[3] , s[63:48] , c[4] , p[1] , g[1]);

// find pg & gg for bigger circuit
cla cla_0(c_in ,p , g , c_out);
endmodule
