// test case a = 4'b0100 , b = 4'b0101 , c_out = 1'b0
module cla_adder_4bit(input [15:0] a,
                      input [15:0] b,
                      input c_in,
                      output [15:0] s,
                      output c_out,
                      output pg,
                      output gg);

wire [3:0] p;
wire [3:0] g;
wire [4:0] c;


// 4 4bit-full adders for 16 bit cla
full_adder_4bit cla_adder_4bit_0(a[3:0] , b[3:0] , c_in , s[3:0] , c[1] , p[0] , g[0]);
full_adder_4bit cla_adder_4bit_1(a[7:4] , b[7:4] , c[1] , s[7:4] , c[2] , p[1] , g[1]);
full_adder_4bit cla_adder_4bit_2(a[11:8] , b[11:8] , c[2] , s[11:8] , c[3] , p[2] , g[2]);
full_adder_4bit cla_adder_4bit_3(a[15:12] , b[15:12] , c[3] , s[7:4] , c[4] , p[1] , g[1]);

// find pg & gg for bigger circuit
cla cla_0(c_in ,p , g , pg ,gg , c_out);
endmodule
