# Computer Architecture Laboratory

ehsan rezaee - 972023015

**list of projects**

    - cla adder 64 bit (project one)
        - full adder
        - cla adder 4,16,64 bit

    - universal shift register (project two)
        - right shift
        - left shift
        - parallel load

    - counter (project three)


> check src folders for schematic circuits 