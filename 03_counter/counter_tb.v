module usv_tb;



    reg[3:0] I;
    reg clk; reg reset;
    reg [1:0] s;
    reg SIFSR; 
    reg SIFSL; 
    wire [3:0]A; 

    universal_shift_register usr(.A(A), .I(I), .clk(clk), .reset(reset), .s(s), .SIFSR(SIFSR), .SIFSL(SIFSL));
    cla_adder_4bit(.a(I) , .b(4'b0100) , .c_in(1'b0) , .s(A)) 


initial begin

    // parallel shift register
    SIFSL = 1'b0;
    reset = 1'b1;
    I = 4'b0101;
    s     = 2'b11;
    
end

always #10 clk = ~clk;

end
